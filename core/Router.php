<?php
    namespace App\Core;

    final class Router {
        private $route = [];

        public function __construct(){}

        public function add(Route $route){
            $this->route[] = $route;
        }

        public function find(string $method, string $url): Route {
            foreach($this->route as $route){
                if($route->matches($method, $url)){
                    return $route;
                }
            }

            return null;
        }
    }