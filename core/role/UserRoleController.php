<?php
    namespace App\Core\Role;

    class UserRoleController extends \App\Core\Controller {
        public function __pre(){
            $userId = $this->getSession()->get('user_id');

            if($userId === null){
                $this->redirect('/PripremnaNastava/user/login',303);
            }
        }
    }