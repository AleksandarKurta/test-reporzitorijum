<?php
    
    namespace App\Controllers;

    class MainController extends \App\Core\Controller {

         public function home(){
            $categoryModel = new \App\Models\CategoryModel($this->getDatabaseConnection());
            $categories = $categoryModel->getAll();
            $this->set('categories', $categories);

            // $this->getSession()->put('neki_kljuc', 'Neka vrednost ' . rand(100, 999));

        }

        public function getRegister(){
       
        }

        public function postRegister(){
            $email = filter_input(INPUT_POST, 'reg_email', FILTER_SANITIZE_EMAIL);
            $forename = filter_input(INPUT_POST, 'reg_forename', FILTER_SANITIZE_STRING);
            $surename = filter_input(INPUT_POST, 'reg_surename', FILTER_SANITIZE_STRING);
            $username = filter_input(INPUT_POST, 'reg_username', FILTER_SANITIZE_STRING);
            $password1 = filter_input(INPUT_POST, 'reg_password_1', FILTER_SANITIZE_STRING);
            $password2 = filter_input(INPUT_POST, 'reg_password_2', FILTER_SANITIZE_STRING);

            if($password1 !== $password2){
                $this->set('message','Doslo je do greske: Niste uneli dva puta istu lozinku');
                return;
            }

            $stringValidator = (new \App\Validators\StringValidator())->setMinLength(7)->setMaxLength(120);
            if(!$stringValidator->isValid($password1)){
                 $this->set('message', 'Doslo je do greske: Lozinka nije ispravnog formata');
                 return;
            }

            $userModel = new \App\Models\UserModel($this->getDatabaseConnection());
            $user = $userModel->getByFieldName('email', $email);
            if($user){
                $this->set('message', 'Doslo je do greske vec postoji korisnik sa tim emailom');
                return;
            }

            $user = $userModel->getByFieldName('username', $username);
            if($user){
                $this->set('message', 'Doslo je do greske vec postoji korisnik sa tim korsnickim imenom');
                return;
            }

            $passwordHash = \password_hash($password1, PASSWORD_DEFAULT);
            $userId = $userModel->add([
                'username' => $username,
                'password_hash' => $passwordHash,
                'email' => $email,
                'forename' => $forename,
                'surename' => $surename
            ]);
            if(!$userId){
                $this->set('message', 'Neuspesno registrovanje naloga');
                return;
            }

            $this->set('message', 'Napravljen je nov nalog sada mozete da se prijavite.');
        }

        public function getLogin(){
            
        }

        public function postLogin(){
            $username = filter_input(INPUT_POST, 'login_username', FILTER_SANITIZE_STRING);
            $password = filter_input(INPUT_POST, 'login_password', FILTER_SANITIZE_STRING);

            $validanPassword = (new \App\Validators\StringValidator())->setMinLength(7)->setMaxLength(120);
            if(!$validanPassword->isValid($password)){
                 $this->set('message', 'Doslo je do greske: Lozinka nije ispravnog formata');
                 return;
            }

            $userModel = new \App\Models\UserModel($this->getDatabaseConnection());

            $user = $userModel->getByFieldName('username', $username);
            if(!$user){
                $this->set('message', 'Doslo je do greske: ne postoji korisnik sa tim korsnickim imenom');
                return;
            }

            if(!password_verify($password, $user->password_hash)){
                sleep(1);
                $this->set('message', 'Doslo je do greske: lozinka nije ispravna');
                return;
            }

            $this->getSession()->put('user_id', $user->user_id);
            $this->getSession()->save();

            $this->redirect( \Configuration::BASE . 'user/profile', 301);
        }
    }